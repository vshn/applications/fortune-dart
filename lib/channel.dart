import 'dart:math';
import 'package:fortune_dart/fortune_dart.dart';
import 'package:mustache_template/mustache_template.dart';

class FortuneDartChannel extends ApplicationChannel {
  @override
  Future prepare() async {
    logger.onRecord.listen(
        (rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"));
  }

// tag::router[]
  @override
  Controller get entryPoint {
    final router = Router();
    final source = File("templates/fortune.html").readAsStringSync();
    final template = Template(source);
    final rng = Random();
    final hostname = Platform.localHostname;
    const version = "1.0-dart";

    router.route("/").linkFunction((request) async {
      var message = "";
      await Process.run('fortune', []).then((ProcessResult pr) {
        message = pr.stdout.toString();
      });
      final random = rng.nextInt(1000);
      final json = {
        'number': random,
        'message': message,
        'version': version,
        'hostname': hostname
      };
      final acceptHeader = request.acceptableContentTypes.first;
      if (acceptHeader.toString() == "text/plain") {
        final text = "Fortune $version cookie of the day #$random:\n\n$message";
        return Response.ok(text)..contentType = ContentType.text;
      } else if (acceptHeader.toString() == "application/json") {
        return Response.ok(json)..contentType = ContentType.json;
      }
      final html = template.renderString(json);
      return Response.ok(html)..contentType = ContentType.html;
    });

    return router;
  }
// end::router[]
}
