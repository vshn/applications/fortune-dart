FROM dart:stable AS build
WORKDIR /app
COPY pubspec.* ./
RUN dart pub get
RUN dart pub global activate conduit

# Copy app source code and AOT compile it.
COPY bin /app/bin
COPY lib /app/lib
RUN dart pub get --offline
RUN ~/.pub-cache/bin/conduit build

# tag::production[]
# Build minimal serving image from AOT-compiled `/server` and required system
# libraries and configuration files stored in `/runtime/` from the build stage.
FROM alpine:3.14
RUN apk update \
    && apk add --no-cache fortune ca-certificates tzdata \
    && rm -rf /var/cache/apk/*
WORKDIR /app/bin
COPY --from=build /runtime/ /
COPY --from=build /app/fortune_dart.aot /app/bin/
COPY templates /app/bin/templates

EXPOSE 8080

# <1>
USER 1001:0

CMD ["/app/bin/fortune_dart.aot", "--port=8080", "--address=0.0.0.0"]
# end::production[]
